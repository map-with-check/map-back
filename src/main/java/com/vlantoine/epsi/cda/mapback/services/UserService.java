package com.vlantoine.epsi.cda.mapback.services;

import com.vlantoine.epsi.cda.mapback.entities.User;
import com.vlantoine.epsi.cda.mapback.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public User create(User user) {
        return this.userRepository.saveAndFlush(user);
    }
}
