package com.vlantoine.epsi.cda.mapback.repositories;

import com.vlantoine.epsi.cda.mapback.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findById(Long id);
    User saveAndFlush(User user);
}
