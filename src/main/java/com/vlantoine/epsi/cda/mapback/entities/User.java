package com.vlantoine.epsi.cda.mapback.entities;

import jakarta.persistence.*;

import java.util.Set;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id",unique=true, nullable = false)
    private Long id;
    private String email = "";
    private String password = "";
    private String login = "";

    @OneToMany(mappedBy = "user")
    private Set<Map> maps;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Set<Map> getMaps() {
        return maps;
    }

    public void setMaps(Set<Map> maps) {
        this.maps = maps;
    }
}
