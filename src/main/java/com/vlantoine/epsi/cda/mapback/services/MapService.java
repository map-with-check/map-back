package com.vlantoine.epsi.cda.mapback.services;

import com.vlantoine.epsi.cda.mapback.entities.Map;
import com.vlantoine.epsi.cda.mapback.entities.User;
import com.vlantoine.epsi.cda.mapback.repositories.MapRepository;
import com.vlantoine.epsi.cda.mapback.repositories.UserRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class MapService {

    private final MapRepository mapRepository;
    private final UserRepository userRepository;

    @Transactional
    public User create(Map map, Long userId) throws Exception {

        Optional<User> user = userRepository.findById(userId);

        if (user.isPresent()) {

            User userUpdated = user.get();
            map.setUser(userUpdated);
            Map newMap = this.mapRepository.saveAndFlush(map);

            Set<Map> userMaps = userUpdated.getMaps();
            userMaps.add(newMap);
            userUpdated.setMaps(userMaps);

            return userUpdated;
        }
        else {
            throw new Exception("user/id");
        }
    }
}
