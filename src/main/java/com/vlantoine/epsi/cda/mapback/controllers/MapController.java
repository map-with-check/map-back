package com.vlantoine.epsi.cda.mapback.controllers;

import com.vlantoine.epsi.cda.mapback.entities.Map;
import com.vlantoine.epsi.cda.mapback.entities.User;
import com.vlantoine.epsi.cda.mapback.services.MapService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin
@AllArgsConstructor
@RestController
public class MapController {

    private final MapService mapService;

    @PostMapping("/user/{userId}/map")
    public User create(@RequestBody Map map, @PathVariable Long userId) {

        try {
            return this.mapService.create(map, userId);
        } catch (Exception e) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "user/id");
        }
    }

}
