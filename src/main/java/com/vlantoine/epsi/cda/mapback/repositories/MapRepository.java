package com.vlantoine.epsi.cda.mapback.repositories;

import com.vlantoine.epsi.cda.mapback.entities.Map;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MapRepository extends JpaRepository<Map, Long> {

    Map saveAndFlush(Map map);
}
