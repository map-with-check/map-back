package com.vlantoine.epsi.cda.mapback.controllers;

import com.vlantoine.epsi.cda.mapback.entities.User;
import com.vlantoine.epsi.cda.mapback.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@AllArgsConstructor
@RestController
public class UserController {

    private final UserService userService;

    @PostMapping("/user")
    public User create(@RequestBody User user) {
        return this.userService.create(user);
    }
}
